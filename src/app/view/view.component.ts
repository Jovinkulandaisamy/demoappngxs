import { Component, OnInit } from '@angular/core';
import { NgForm, AbstractControl, FormControl } from '@angular/forms';
import {Form} from '../form/form.model';
import{Cart}  from '../cart/cart.model';
import {FormState} from '../form/form.state';
import{CartState} from '../cart/cart.state';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {AddForm,DeleteForm, GetForms, SetSelectedForm} from '../form/form.action';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
 @Select(FormState.getFormList) formList: Observable<Form[]>;
  @Select(CartState.getCartList) cartList: Observable<Cart[]>;
  constructor(private store:Store) { }

  ngOnInit(): void {
  }

  onEdit(fi:Form){
  this.store.dispatch(new SetSelectedForm(fi));
 //console.log("formOne",this.formOne);
  //this.refreshForm();
}

onDelete(_id: string){
  if (confirm('Are you sure to delete this record ?') == true) {
   this.store.dispatch(new DeleteForm(_id));
  }
  /*if (confirm('Are you sure to delete this record ?') == true) {
      this.formService.deleteForm(_id).subscribe((res) => {
        if(res=="Deleted"){
          console.log("Deleted Successfully");
        }else{
          console.log("Error occured while deleting the document");
        }
          this.refreshForm();
      });
  }*/
}

}
