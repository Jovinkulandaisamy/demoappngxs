import { Component, OnInit } from '@angular/core';
import { NgForm, AbstractControl, FormControl } from '@angular/forms';
import {Form} from './form.model';
import {FormService} from './form.service';
import {FormState} from './form.state';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {AddForm,DeleteForm, GetForms, SetSelectedForm} from './form.action';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {
    @Select(FormState.getFormList) formList: Observable<Form[]>;
     @Select(FormState.getSelectedForm) formOne: Observable<Form>;
//formList : Form[];
  constructor(public formService:FormService,private store: Store) { 
  }

  ngOnInit(): void {
    /*this.formOne.subscribe(a => {
            if (a) {
              this.formService.selectedForm.patchValue({
                _id: a._id,
                formName: a.formName,
                formDescription: a.formDescription,
                formValue:a.formValue
              });
            };
     });*/
    this.reset();
    this.refreshForm();
  }

reset(form?: NgForm){
  if(form){
    form.reset();
  }
  this.formService.selectedForm = {
    _id:"",
    formName:"",
    formDescription:"",
    formValue:""
  }
  
}

onEdit(fi:Form){
  this.store.dispatch(new SetSelectedForm(fi));
 console.log("formOne",this.formOne);
  this.refreshForm();
}

onDelete(_id: string){
  if (confirm('Are you sure to delete this record ?') == true) {
   this.store.dispatch(new DeleteForm(_id));
  }
  /*if (confirm('Are you sure to delete this record ?') == true) {
      this.formService.deleteForm(_id).subscribe((res) => {
        if(res=="Deleted"){
          console.log("Deleted Successfully");
        }else{
          console.log("Error occured while deleting the document");
        }
          this.refreshForm();
      });
  }*/
}

   onSubmit(form: NgForm) {
      this.store.dispatch(new AddForm(form.value));
/*this.formService.createForm(form.value).subscribe((res) => {
this.reset(form);
this.refreshForm();
   });*/
   this.reset(form);
//this.refreshForm();
   }

   refreshForm(){
      this.store.dispatch(new GetForms());
     //this.formService.getForm().subscribe((res)=>{
//this.formList = res as Form[];
  //   });
   }

}
