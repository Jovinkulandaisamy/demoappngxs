import { Injectable } from '@angular/core';
import{Form} from './form.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FormService {
selectedForm :Form;
readonly baseURL = 'http://localhost:3000' +'/forms';

  constructor(private http:HttpClient) { }

createForm(form:Form){
return this.http.post(this.baseURL,form);
}

getForm(){
return this.http.get(this.baseURL);
}

deleteForm(_id:string){
return this.http.delete(this.baseURL+`/${_id}`);
}


}
