import {State, Action, StateContext, Selector} from '@ngxs/store';
import { Injectable } from '@angular/core';
import {Form} from './form.model';
import {AddForm, DeleteForm, GetForms, SetSelectedForm, UpdateForm} from './form.action';
import {FormService} from './form.service';
import {tap} from 'rxjs/operators';

export class FormStateModel {
    forms: Form[];
    selectedForm: Form;
}

@State<FormStateModel>({
    name: 'forms',
    defaults: {
        forms: [],
        selectedForm: null
    }
})

@Injectable()
export class FormState {

    constructor(private formService: FormService) {
    }

    @Selector()
    static getFormList(state: FormStateModel) {
        return state.forms;
    }

        @Selector()
    static getSelectedForm(state: FormStateModel) {
        return state.selectedForm;
    }

    @Action(GetForms)
    getForms({getState, setState}: StateContext<FormStateModel>) {
        return this.formService.getForm().pipe(tap((result) => {
            const state = getState();
            setState({
                ...state,
                forms: result as Form[],
            });
        }));
    }

    @Action(AddForm)
    addForm({getState, patchState}: StateContext<FormStateModel>, {payload}: AddForm) {
        return this.formService.createForm(payload).pipe(tap((result) => {
            const state = getState();
            patchState({
                forms: [...state.forms, result as Form]
            });
        }));
    }


    @Action(DeleteForm)
    deleteForm({getState, setState}: StateContext<FormStateModel>, {_id}: DeleteForm) {
        return this.formService.deleteForm(_id).pipe(tap(() => {
            const state = getState();
            const filteredArray = state.forms.filter(item => item._id !== _id);
            setState({
                ...state,
                forms: filteredArray,
            });
             console.log("forms",filteredArray);
        }));
    }

        @Action(SetSelectedForm)
    setSelectedFormId({getState, setState}: StateContext<FormStateModel>, {payload}: SetSelectedForm) {
        const state = getState();
        setState({
            ...state,
            selectedForm: payload
        });
    }

}