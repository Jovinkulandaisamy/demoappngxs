import {Form} from './form.model';

export class AddForm {
    static readonly type = '[Form/] Add';

    constructor(public payload: Form) {
    }
}

export class GetForms {
    static readonly type = '[Form] Get';
}

export class UpdateForm {
    static readonly type = '[Form] Update';

    constructor(public payload: Form, public id: number) {
    }
}

export class DeleteForm {
    static readonly type = '[Form] Delete';

    constructor(public _id: string) {
    }
}

export class SetSelectedForm {
    static readonly type = '[Form] Set';

    constructor(public payload: Form) {
    }
}