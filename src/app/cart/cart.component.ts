import { Component, OnInit } from '@angular/core';
import{CartService} from './cart.service';
import { NgForm, AbstractControl, FormControl } from '@angular/forms';
import {Cart} from './cart.model';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {AddCart, GetCarts} from './cart.action';
import{CartState} from './cart.state';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
//cartList : Cart[];
  @Select(CartState.getCartList) cartList: Observable<Cart[]>;
  constructor(public cartService:CartService,private store:Store) { }

  ngOnInit(): void {
    this.reset();
    this.refreshCart();
  }

  reset(form?:NgForm){
     if(form){
    form.reset();
  }
    this.cartService.selectedCart = {
    _id:"",
    itemName:"",
    itemDescription:"",
    quantity:""
    }

    
  }

  onSubmit(form:NgForm){
    this.store.dispatch(new AddCart(form.value));
//this.cartService.createCart(form.value).subscribe((res) => {
this.reset(form);
this.refreshCart();
  // });
  }

  refreshCart(){
    this.store.dispatch(new GetCarts());
    // this.cartService.getCart().subscribe((res)=>{
//this.cartList = res as Cart[];
  //  });
  }

}
