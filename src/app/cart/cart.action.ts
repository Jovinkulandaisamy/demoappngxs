import {Cart} from './cart.model';

export class AddCart {
    static readonly type = '[Cart] Add';

    constructor(public payload: Cart) {
    }
}

export class GetCarts {
    static readonly type = '[Cart] Get';
}
