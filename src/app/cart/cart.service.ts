import { Injectable } from '@angular/core';
import{Cart} from './cart.model';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CartService {
selectedCart : Cart;
readonly baseURL = 'http://localhost:3000' +'/carts';
  constructor(private http:HttpClient) { }

  getCart(){
    return this.http.get(this.baseURL);
  }

  createCart(cart:Cart){
    return this.http.post(this.baseURL,cart);
  }


}
