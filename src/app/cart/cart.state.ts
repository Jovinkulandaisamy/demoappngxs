import {State, Action, StateContext, Selector} from '@ngxs/store';
import { Injectable } from '@angular/core';
import {Cart} from './cart.model';
import {AddCart, GetCarts} from './cart.action';
import {CartService} from './cart.service';
import {tap} from 'rxjs/operators';

export class CartStateModel {
    carts: Cart[];
    selectedCart: Cart;
}

@State<CartStateModel>({
    name: 'carts',
    defaults: {
        carts: [],
        selectedCart: null
    }
})

@Injectable()
export class CartState {

    constructor(private cartService: CartService) {
    }

    @Selector()
    static getCartList(state: CartStateModel) {
        return state.carts;
    }

    @Action(GetCarts)
    getCarts({getState, setState}: StateContext<CartStateModel>) {
        return this.cartService.getCart().pipe(tap((result) => {
            const state = getState();
            setState({
                ...state,
                carts: result as Cart[],
            });
        }));
    }

    @Action(AddCart)
    addCart({getState, patchState}: StateContext<CartStateModel>, {payload}: AddCart) {
        return this.cartService.createCart(payload).pipe(tap((result) => {
            const state = getState();
            patchState({
                carts: [...state.carts, result as Cart]
            });
        }));
    }


}